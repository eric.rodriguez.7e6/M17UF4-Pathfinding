using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject token1, token2, token3, token4;
    private int[,] gameMatrix; //0 not chosen, 1 player, 2 enemy
    private int[] startPos = new int[2];
    private int[] objectivePos = new int[2];

    public List<NodeEric> llistaOberta = new();
    public List<NodeEric> llistaTancada = new();

    private void Awake()
    {
        gameMatrix = new int[Calculator.length, Calculator.length];

        for (int i = 0; i < Calculator.length; i++) //fila
            for (int j = 0; j < Calculator.length; j++) //columna
                gameMatrix[i, j] = 0;

        //randomitzar pos final i inicial;
        var rand1 = Random.Range(0, Calculator.length);
        var rand2 = Random.Range(0, Calculator.length);
        startPos[0] = rand1;
        startPos[1] = rand2;
        SetObjectivePoint(startPos);

        gameMatrix[startPos[0], startPos[1]] = 1;
        gameMatrix[objectivePos[0], objectivePos[1]] = 2;

        InstantiateToken(token1, startPos);
        InstantiateToken(token2, objectivePos);
        //ShowMatrix();

        llistaTancada.Add(new(startPos, objectivePos, new List<int[]>(), new int[] { -1, -1 }, 0));
        AddToList(llistaTancada[^1].AvailableSiblings());
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            llistaOberta.Clear();
            llistaTancada.Clear();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
    private void AddToList(IEnumerable<NodeEric> list)
    {
        foreach (var item in list)
        {
            if (llistaTancada.Any(item2 => item2.pos == item.pos)) return;

            List<NodeEric> node = llistaOberta.Where(item2 => item2.pos == item.pos).ToList();
            if(node.Count > 0 && node[0].cost + node[0].heuristic < item.cost + item.heuristic)
            {
                llistaOberta.Remove(node[0]);
                llistaOberta.Add(item);
                return;
            }
            llistaOberta.Add(item);
        }
        if(llistaOberta.Any(item => item.heuristic == 0))
        {
            NodeEric nodeFinal = llistaOberta.Where(item => item.heuristic == 0).ToArray()[0];
            for (int i = 0; i < llistaOberta.Count; i++)
            {
                int[] nodePos = new int[] { llistaOberta[i].pos[0], llistaOberta[i].pos[1] };
                if (!(nodePos[0] == objectivePos[0] && nodePos[1] == objectivePos[1]) &&
                    !(nodePos[0] == startPos[0] && nodePos[1] == startPos[1]))
                {
                    GameObject node = Instantiate(token4, Calculator.GetPositionFromMatrix(new int[] { nodePos[0], nodePos[1] }) - new Vector3(0, 0, 1), new Quaternion(0, 0, 0, 0));
                    if (nodeFinal.historic.Where(item => item[0] == nodePos[0] && item[1] == nodePos[1]).ToList().Count > 0) node.GetComponent<SpriteRenderer>().color = new(255,0,0);
                }
            }
            return;
        }
        if(llistaOberta.Count > 0)
        {
            llistaTancada.Add(llistaOberta.OrderBy(item => item.cost + item.heuristic).ToArray()[0]);
            AddToList(llistaTancada[^1].AvailableSiblings());
        }
    }
    private void InstantiateToken(GameObject token, int[] position)
    {
        Instantiate(token, Calculator.GetPositionFromMatrix(position),
            Quaternion.identity);
    }
    private void SetObjectivePoint(int[] startPos)
    {
        // Eric: Debido a la (baja) posibilidad de que ambos random saliesen {0,0} y quedasen sobrepuestos, lo he puesto en un do while xd
        int rand1, rand2;
        do
        {
            rand1 = Random.Range(0, Calculator.length);
            rand2 = Random.Range(0, Calculator.length);
        } while (new int[] { rand1, rand2 } == startPos);

        objectivePos[0] = rand1;
        objectivePos[1] = rand2;
    }

    private void ShowMatrix() //fa un debug log de la matriu
    {
        string matrix = "";
        for (int i = 0; i < Calculator.length; i++)
        {
            for (int j = 0; j < Calculator.length; j++)
            {
                matrix += gameMatrix[i, j] + " ";
            }
            matrix += "\n";
        }
        Debug.Log(matrix);
    }
}
