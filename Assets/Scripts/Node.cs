using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using Unity.Mathematics;
using UnityEngine;
public class NodeEric
{
    public int[] pos;
    public int[] obj;
    public int cost;
    public float heuristic;
    public List<int[]> historic;
    public List<int[]> childNodes;

    // M�todos
    public NodeEric AddChild(int[] pos, int[] obj)
    {
        NodeEric nouNode = new(pos, obj, historic, this.pos, cost);
        childNodes.Add(nouNode.pos);
        return nouNode;
    }
    
    public int[] SelectChild(List<NodeEric> nodes)
    {
        int[] selected = nodes[0].pos;
        float minCost = nodes[0].cost + nodes[0].heuristic;
        foreach (var item in nodes.Where(item => AreSiblings(item.pos, pos)))
        {
            if(minCost > item.cost + item.heuristic)
            selected = item.pos;
        }
        return selected;
    }
    public List<NodeEric> AvailableSiblings()
    {
        List<NodeEric> siblings = new();
        if (pos[0] + 1 < Calculator.length) siblings.Add(NewNode(new int[] { pos[0] + 1, pos[1] }));
        if (pos[1] + 1 < Calculator.length) siblings.Add(NewNode(new int[] { pos[0], pos[1] + 1 }));
        if (pos[0] - 1 >= 0) siblings.Add(NewNode(new int[] { pos[0] - 1, pos[1] }));
        if (pos[1] - 1 >= 0) siblings.Add(NewNode(new int[] { pos[0], pos[1] - 1 }));

        return siblings;
    }
    private NodeEric NewNode(int[] pos)
    {
        return new(pos, obj, historic, pos, 1);
    }

    //Constructor
    public NodeEric(int[] pos, int[] obj, List<int[]> historic, int[] father, int cost)
    {
        this.pos = pos;
        this.obj = obj;
        this.historic = new(historic);
        if(father != new int[] {-1,-1}) this.historic.Add(father);
        this.cost = cost + 1;
        heuristic = Calculator.CheckDistanceToObj(pos, obj);
        //heuristic = Mathf.Abs(Vector2.Distance(new(obj[0], obj[1]), new(pos[0], pos[1])));
        //Debug.Log($"Nodo {Int2(pos)} a {Int2(obj)}, ha pasado: {historic.Count}, padre: {Int2(father)}, coste total: {cost}, distancia: {heuristic}");
        //var a = Object.Instantiate(GameObject.Find("NodeNoSprite"), Calculator.GetPositionFromMatrix(new int[] { pos[0], pos[1]}), new Quaternion(0,0,0,0));
        //a.name = $"Nodo({pos[0]},{pos[1]})";
    }

    // Utilidad
    static string Int2(int[] a)
    {
        return $"({a[0]},{a[1]})";
    }
    static bool AreSiblings(int[] a, int[] b)
    {
        float diffX = a[0] - b[0],
            diffY = a[1] - b[1];
        return (diffX <= 1 && diffX >= 1) && (diffY <= 1 && diffY >= 1);
    }
}
